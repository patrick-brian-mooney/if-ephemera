# Patrick Mooney's IF ephemera

Miscellaneous leftovers from my encounters with various pieces of IF.

Index to these files is [here](https://patrickbrianmooney.nfshost.com/~patrick/IF/by_author/).

Collector's homepage is [here](https://patrickbrianmooney.nfshost.com/~patrick/).


