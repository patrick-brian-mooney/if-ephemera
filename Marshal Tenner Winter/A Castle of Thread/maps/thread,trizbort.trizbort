﻿<?xml version="1.0" encoding="utf-8"?>
<trizbort>
	<info />
	<map>
		<room id="2" name="Guest Cabin" x="-512" y="672" w="96" h="64" description="This guest cabin barely gives one any real comfort, but on a no-frills boat such as this you're lucky to even have a room to yourself.  There is no window as everything below deck is also below the bilge line.  As the barge moves through the muddy swamp waters, a constant and hypnotic slushing sound gives the cabin a strange, claustrophobic feel.">
			<objects>mysterious note|haversack|    Elements of Ixteesh|    Phandaal's letter|glowstone lantern|bed|chair|desk</objects>
		</room>
		<room id="1" name="Centerline Passage Fore" x="-352" y="672" w="96" h="64" description="This is the fore end of the centerline passageway that runs down the center of the barge, belowdecks.  The craft is heading north and this end of the passage has doors all around but to the south, where the passage continues toward the stern.  A short ramp slopes upwards to the northeast, taking one topside.">
			<objects>ironwood table|    old glowcube|Venkath|Rakton</objects>
		</room>
		<line id="9">
			<dock index="0" id="2" port="e" />
			<dock index="1" id="1" port="w" />
		</line>
		<room id="7" name="Starboard Fore Deck" x="-64" y="384" w="96" h="64" description="You're standing on the deck of The Rhinoceros.  From here, you can see the extent of this barge-like craft.  This deck is low to the water level as most of the ship is underwater.  There is no railing as it's easier to move large cargo onboard that way.  The only structure on the deck is toward the very bow (north).  It's the wheelhouse, where the captain pilots the craft.  The door into it is closed.  Around the other parts of the deck, south, west, and southwest, you can see various crates of cargo stacked.  At the very back of the ship, you can see the wide stern wheel ceaselessly paddling the ship northwards, rhythmically chugging through the desolate, waterlogged swamp.  A wide, short ramp leads down to belowdecks.  It is stained from boot traffic and swamp water.">
			<objects>crates of cargo</objects>
		</room>
		<line id="15" startText="up" endText="down">
			<dock index="0" id="1" port="ene" />
			<dock index="1" id="7" port="ssw" />
		</line>
		<room id="12" name="Port Fore Deck" x="-224" y="384" w="96" h="64" description="This part of the deck is up by the wheelhouse and on the port side of the craft.  From here, you can see the extent of this barge-like craft. This deck is low to the water level as most of the ship is underwater. There is no railing as it's easier to move large cargo onboard that way.  The deck continues east, southeast, and south, where various cargo crates are stacked.">
			<objects>Ostothas</objects>
		</room>
		<line id="20">
			<dock index="0" id="7" port="w" />
			<dock index="1" id="12" port="e" />
		</line>
		<room id="21" name="Port Cargo Deck" x="-224" y="512" w="96" h="64" description="This is the port cargo deck of The Rhinoceros.  From here, you can see the extent of this barge-like craft. This deck is low to the water level as most of the ship is underwater. There is no railing as it's easier to move large cargo onboard that way. The only structure on the deck is toward the very bow (north). It's the wheelhouse, where the captain pilots the craft. The door into it is closed. Around the other parts of the deck, north, northeast, and east, you can see various crates of cargo stacked. At the very back of the ship, you can see the wide stern wheel ceaselessly paddling the ship northwards, rhythmically chugging through the desolate, waterlogged swamp." />
		<line id="25">
			<dock index="0" id="12" port="s" />
			<dock index="1" id="21" port="n" />
		</line>
		<line id="24">
			<dock index="0" id="21" port="ne" />
			<dock index="1" id="7" port="sw" />
		</line>
		<room id="13" name="Starboard Cargo Deck" x="-64" y="512" w="96" h="64" description="This part of the deck is the starboard cargo deck.  From here, you can see the extent of this barge-like craft. This deck is low to the water level as most of the ship is underwater. There is no railing as it's easier to move large cargo onboard that way. The only structure on the deck is toward the very bow (north). It's the wheelhouse, where the captain pilots the craft. The door into it is closed. Around the other parts of the deck, north, west, and northwest, you can see various crates of cargo stacked. At the very back of the ship, you can see the wide stern wheel ceaselessly paddling the ship northwards, rhythmically chugging through the desolate, waterlogged swamp.  On the edge of the deck here, a stovepipe juts upward from the galley below.">
			<objects>stovepipe</objects>
		</room>
		<line id="31">
			<dock index="0" id="12" port="se" />
			<dock index="1" id="13" port="nw" />
		</line>
		<line id="18">
			<dock index="0" id="13" port="n" />
			<dock index="1" id="7" port="s" />
		</line>
		<line id="11">
			<dock index="0" id="13" port="w" />
			<dock index="1" id="21" port="e" />
		</line>
		<room id="10" name="Wheelhouse" x="-64" y="-32" w="96" h="64" description="This forward enclosure is the wheelhouse of the bilgescow.  Glassless windows open the walls forward, port, and starboard and the door to the rest of the boat is stern (south).  The eponymous wheel is one solid piece of red oak and is fixed in the center of the forward opening. A small closed hatch in the floor leads below deck.">
			<objects>Tauzius|wheel</objects>
		</room>
		<line id="27">
			<dock index="0" id="7" port="n" />
			<dock index="1" id="10" port="s" />
		</line>
		<room id="26" name="Captain's Cabin" x="-352" y="224" w="96" h="64" description="Not opulent but certainly a step-up from the cabin you were given, the captain's room is a large cabin situated below the ship's wheelhouse.  In fact, a short spiral staircase leads up to it through a small hatch.  The bed is a simple mattress within a heavy wooden frame.  An actual ironwood roll-top desk sits against a wall.  A heavy chair sits before it.  Over the desk, a detailed map of the swamp waterways hangs.  Finally, a wide trunk sits nearby and an exit is to the south.">
			<objects>bed|ironwood desk|    slide rule|heavy chair|map of swamp waterways|trunk</objects>
		</room>
		<line id="28" style="dashed">
			<dock index="0" id="1" port="n" />
			<dock index="1" id="26" port="s" />
		</line>
		<line id="32" startText="down" endText="up">
			<dock index="0" id="10" port="sw" />
			<dock index="1" id="26" port="ne" />
		</line>
		<room id="3" name="Centerline Passage Amidships" x="-352" y="896" w="96" h="64" description="You are in the centerline passage running from the bow to the stern belowdecks.  At this section, there are doors both port and starboard.  Otherwise, you can head up or down the passage, from stem to stern.  The air down here is thick with moisture and much of the wood around you is damp with condensation." />
		<line id="14">
			<dock index="0" id="1" port="s" />
			<dock index="1" id="3" port="n" />
		</line>
		<room id="5" name="Crew Quarters" x="-512" y="896" w="96" h="64" description="A disagreeable smell permeates this room.  Hammocks and low-hanging ropes are haphazardly arranged for the crew's sleeping needs.  Random bits of clothes and personal items are also strewn about.  The only exit is a door to the east (starboard).">
			<objects>Daxas|hammocks|    clothes</objects>
		</room>
		<line id="16">
			<dock index="0" id="3" port="w" />
			<dock index="1" id="5" port="e" />
		</line>
		<room id="4" name="Scow Galley" x="-192" y="896" w="96" h="64" description="This small kitchenette is provided to prepare simple meals for the barge's crew.  A small table is here; its top covered by a thick chopping block, marred from ages of use.  A note of some sort is on it.  A wide cupboard is fastened to one wall and a cast iron stove is against another.  The stove's door is closed.  The only exit is port (or west, currently).">
			<objects>small table|    important note|cupboard|    food|    cheese|stove|    (bird's nest)|        old key</objects>
		</room>
		<line id="17">
			<dock index="0" id="3" port="e" />
			<dock index="1" id="4" port="w" />
		</line>
		<room id="6" name="Centerline Passage Astern" x="-352" y="1088" w="96" h="64" description="This is the end of the centerline passage, at the stern of the barge.  There are doors port, starboard, and astern (south) here.  A faded wooden plaque with writing on it is fastened to the stern door.  Otherwise, you can head back north toward the bow.  Here, below the bilge line, the air is thick with moisture and much of the wood around you is damp with condensation.">
			<objects>plaque</objects>
		</room>
		<line id="30">
			<dock index="0" id="3" port="s" />
			<dock index="1" id="6" port="n" />
		</line>
		<room id="19" name="Storage Closet" x="-512" y="1088" w="96" h="64" description="This cabin is too small to be someone's quarters so instead it's used for random storage.  Currently, only a large chest sits against a wall here.  The only door is to the east (starboard).">
			<objects>chest|    cabin key</objects>
		</room>
		<line id="34">
			<dock index="0" id="6" port="w" />
			<dock index="1" id="19" port="e" />
		</line>
		<room id="22" name="Cabin Boy's Quarters" x="-192" y="1088" w="96" h="64" description="This cabin is barely bigger than a broom closet.  A patchwork hammock fills most of the space, strung between two of the walls.  An old pillow sits on the floor in a corner, obviously now a bed for the ship's cats.  Nearby, a food bowl and a water dish are provided, again, for the cats.  The only exit is port (or west, currently).">
			<objects>hammock|    Amalek|bowl</objects>
		</room>
		<line id="35">
			<dock index="0" id="6" port="e" />
			<dock index="1" id="22" port="w" />
		</line>
		<room id="8" name="First Mate's Cabin" x="-192" y="672" w="96" h="64" description="This cabin is a step-up in comfort than for the rest of the crew.  The wooden plank bed has a simple pillow and a nicely folded quilt on it.  A plank shelf, against another wall, is covered with neatly stacked papers.  A heavy, wooden chair is before it.  The first mate's personal belongings are most likely kept in the small trunk nearby.  The exit is port (west).">
			<objects>plank bed|    pillow|    quilt|shelf|    papers|chair|trunk|    Owls of Stone|        coded message</objects>
		</room>
		<line id="38" style="dashed">
			<dock index="0" id="1" port="e" />
			<dock index="1" id="8" port="w" />
		</line>
		<room id="23" name="Ivory Arch" x="800" y="704" w="96" h="64" description="It is a single piece of ivory, huge enough to span from one side of the boulevard to the other and no man lives who knows whence so vast a piece could have come.  Passing through the arch, one finds the entire underside covered in countless, detailed carvings; mostly statues and visages that seem to reach out from the ivory or stare at you as you pass through.  To the north, beyond the ivory arch, the buidlings give way to some sort of open town square." />
		<room id="33" name="Lord Phothice's Square" x="800" y="576" w="96" h="64" description="This town square is hardly a square and more of an askew hexagon.  Its dimensions are difficult to tell as the surrounding buildings crowd each other for space.  Avenues lead off in almost every direction.  The southern boulevard leads under a gigantic ivory arch with an underside covered in countless carvings.  The way west is blocked by a spiked gate.  Beyond it is the courtyard of the inn known as The Vulgar Unicorn.  Even in your village, the far away Euyesus, you've heard a few tales of the tavern's rowdiness." />
		<line id="47">
			<dock index="0" id="23" port="n" />
			<dock index="1" id="33" port="s" />
		</line>
		<room id="36" name="Inn Courtyard" x="640" y="576" w="96" h="64" description="Nestled within the buildings of Badushizd, this stony courtyard serves the patrons of The Vulgar Unicorn as a place to enjoy if the tavern gets too noisy.  A dry fountain sits as a centerpiece, surrounded by four stone benches with areas of trimmed grass in between.  An open spiked gate leads out of this courtyard to the east and a shadowy break in the wall to the northwest leads to some sort of dark alley between the buildings.  The door into the tavern is to the west.">
			<objects>dry fountain|stone benches|periwinkle sadiki bird|lavender sadiki bird|spiked gate</objects>
		</room>
		<line id="46">
			<dock index="0" id="33" port="w" />
			<dock index="1" id="36" port="e" />
		</line>
		<room id="49" name="Tavern Common Room" x="480" y="576" w="96" h="64" description="Bright and shadowy, all at once, this tavern common room is covered in countless burning candles of various shapes, sizes, and colors.  This perpetual candlelight has been happening for so long, the floor is covered in dry candle wax, stamped into a dust that has become one with the floor.  The wooden walls and ceiling are completely covered in soot and the patrons through the many decades have taken it upon themselves to scrawl graffiti in it with their fingers and knives.">
			<objects>burning candles|dust|bar|spirits|tables and chairs|Paevana the alewife ("young woman")|(Sceldrus Jandol)</objects>
		</room>
		<line id="50">
			<dock index="0" id="36" port="w" />
			<dock index="1" id="49" port="e" />
		</line>
		<room id="40" name="Dark Alley" x="480" y="448" w="96" h="64" description="This alley is generally crooked, of course, and the way it is sandwiched between the buildings blocks almost any natural light, leaving  this passage in shadows.  The alley leads southeast (back to the inn courtyard) and southwest (deeper into shadow).">
			<objects>(thug)</objects>
		</room>
		<line id="53">
			<dock index="0" id="36" port="nw" />
			<dock index="1" id="40" port="se" />
		</line>
		<room id="52" name="Dead End" x="320" y="576" w="96" h="64" description="The dark alley stops here at a dead end.  The angles of this cul-de-sac are contorted so the dark walls are disorienting to make out.  The east border is the back of The Vulgar Unicorn, its face covered with a gray city ivy known as shalifux,  A round window is up the wall on the second floor.  Across from the back of the tavern, a tall brick wall blocks what was once another alley.  The only exit is back down the dark alley to the northeast." />
		<line id="58">
			<dock index="0" id="40" port="sw" />
			<dock index="1" id="52" port="ne" />
		</line>
		<room id="37" name="Upstairs Hall" x="800" y="256" w="96" h="64" description="This is the upstairs hall of The Vulgar Unicorn.  From below, the sounds of the tavern common room rises up the wooden staircase.  Bedroom doors are set east and west and a lavatory door is to the south.  The soot graffiti continues on this floor, as does the smoky aroma of tobacco." />
		<line id="44" startText="up" endText="down">
			<dock index="0" id="49" port="ne" />
			<dock index="1" id="37" port="sw" />
		</line>
		<room id="39" name="Pipe-Filled Lavatory" x="800" y="384" w="96" h="64" description="This strange room is some sort of lavatory, but not like any that you've seen back in Euyesus.  There's a commode made of polished wood with a small handle nearby. There's also a porcelain sink, but the commode has numerous pipes extending from it and clinging to the wall before disappearing into it and the sink includes a faucet which is a brass spigot also attached to a network of pipes.  Finally, there is an old mirror on another wall, affixed between more pipes.  The only exit is the door to the north.">
			<objects>commode|    handle|sink|    faucet|pipes|mirror</objects>
		</room>
		<line id="45">
			<dock index="0" id="37" port="s" />
			<dock index="1" id="39" port="n" />
		</line>
		<room id="43" name="Back Bedroom" x="160" y="256" w="96" h="64" description="This room is a chaotic mess.  The bed frame and mattress is crooked as if someone pushed it or was pushed against it.  A sofa is overturned as if flung by a giant hand.  A blood-stained quilt is bunched up on the floor.  On the west wall, half of a curtain hangs askew, partially covering a round window.  The rest of the curtain is on the floor.  There is a closed door to the east.">
			<objects at="w">Deviah Favash|mattress|bed frame|sofa|curtain|round window|quilt</objects>
		</room>
		<line id="56" style="dashed" midText="climb ivy">
			<dock index="0" id="52" port="nw" />
			<dock index="1" id="43" port="se" />
		</line>
		<line id="42" style="dashed">
			<dock index="0" id="43" port="e" />
			<dock index="1" id="37" port="w" />
		</line>
		<room id="41" name="Tavern Cellar" x="480" y="768" w="96" h="64" description="This space is crowded with decades upon decades of tavern flotsam; broken chairs, upended tables, crates of empty bottles, among many other dusty things.  The only exit is a staircase that leads up to the tavern common room, behind the curved bar.">
			<objects>(Paevana alewife)|chairs|crates|    bottles|tables</objects>
		</room>
		<line id="48" startText="down" endText="up">
			<dock index="0" id="49" port="ssw" />
			<dock index="1" id="41" port="nnw" />
		</line>
		<room id="51" name="Sloping Path" x="1504" y="1440" w="96" h="64" description="The long trail you've been following, if one could even call it a trail, slopes upward from here to the north.  Badushizd is now days to the south, where the snow and ice are not eternal and the sun actually peers through the clouds.  Obak, however, you've come to find is a gray and frozen wasteland.  Around the faint path up the slope are a good many spruce trees, frosted with snow." />
		<line id="54">
			<dock index="0" id="55" port="s" />
			<dock index="1" id="51" port="n" />
		</line>
		<room id="55" name="Harbor Overlook" x="1504" y="1312" w="96" h="64" description="This is the crest of a small, rounded snow-covered hill dotted with large spruce trees.  From here, you can see to the north a magnificent and enormous frozen harbor.  The skeletons of various ships and boats are half-sunk in its surface, as if frozen in different states of destruction and submersion.  Something happened long ago in this wasteland that was once a green kingdom, if the stories are to be believed.  From up on this snowy hill, some paths slope downward; west, south, and east." />
		<line id="60">
			<dock index="0" id="63" port="w" />
			<dock index="1" id="55" port="e" />
		</line>
		<room id="57" name="Rocky Path" x="1344" y="1312" w="96" h="64" description="This is a rocky path, covered with snow, of course.  It raises upwards to the east and downwards to the northeast.  A handful of spruce trees dot the area." />
		<line id="59">
			<dock index="0" id="57" port="e" />
			<dock index="1" id="55" port="w" />
		</line>
		<room id="61" name="Harbor's Edge" x="1504" y="1184" w="96" h="64" description="This is the very edge of the frozen harbor.  It's expansive, icy waste stretches out to the northwest, north, and northeast.  Ahead that way, you can see the dark silhouettes of the skeletal remains of ships, large and small, half-sunk in the harbor's ice.  To the south is a snow-covered hill.  Paths leading southeast and southwest wind up to its crest.  The scattered spruce trees end here at the edge of the frozen harbor." />
		<line id="66">
			<dock index="0" id="57" port="ne" />
			<dock index="1" id="61" port="sw" />
		</line>
		<room id="63" name="Winding Path" x="1664" y="1312" w="96" h="64" description="This snowy path winds up to the west and down to the northwest, between a few stately spruce trees." />
		<line id="69">
			<dock index="0" id="63" port="nw" />
			<dock index="1" id="63" port="nw" />
		</line>
		<line id="70">
			<dock index="0" id="61" port="se" />
			<dock index="1" id="63" port="nw" />
		</line>
		<room id="62" name="Crossing the Ice" x="1504" y="1056" w="96" h="64" description="You are crossing over the ice of the frozen harbor.  The gray weather has limited your visibility and you can only really make out the shore and the hill to the south and the shadow of a half-sunk ship can be seen ahead to the north." />
		<line id="65">
			<dock index="0" id="61" port="n" />
			<dock index="1" id="62" port="s" />
		</line>
		<room id="67" name="Half-Sunk Cutter (near stern)" x="1504" y="832" w="96" h="64" description="" />
		<line id="68">
			<dock index="0" id="67" port="s" />
			<dock index="1" id="62" port="n" />
		</line>
		<room id="71" name="Half-Sunk Cutter (near topside)" x="1664" y="704" w="96" h="64" description="" />
		<line id="72">
			<dock index="0" id="67" port="ne" />
			<dock index="1" id="71" port="sw" />
		</line>
		<room id="73" name="Half-Sunk Cutter (near keel)" x="1344" y="704" w="96" h="64" description="" />
		<line id="74">
			<dock index="0" id="67" port="nw" />
			<dock index="1" id="73" port="se" />
		</line>
		<room id="75" name="Half-Sunk Cutter (near bow)" x="1504" y="576" w="96" h="64" description="" />
		<line id="76">
			<dock index="0" id="73" port="ne" />
			<dock index="1" id="75" port="sw" />
		</line>
		<line id="77">
			<dock index="0" id="71" port="nw" />
			<dock index="1" id="75" port="se" />
		</line>
		<room id="78" name="Crossing More Ice" x="1504" y="384" w="96" h="64" description="" />
		<line id="79">
			<dock index="0" id="75" port="n" />
			<dock index="1" id="78" port="s" />
		</line>
		<room id="80" name="Research Camp (south of the stone)" x="1504" y="256" w="96" h="64" description="" />
		<line id="81">
			<dock index="0" id="78" port="n" />
			<dock index="1" id="80" port="s" />
		</line>
		<room id="82" name="Badushizd Caravansary" x="1344" y="256" w="96" h="64" description="This part of the research camp is the home base of the envoy from Badushizd.  There are four large tents here, generally surrounding a small, raised pyre, aflame and casting warmth and dancing shadows around the camp.  The rest of the research camp is to the east.">
			<objects>Dhag Sepheres|Cavender Quelan</objects>
		</room>
		<line id="83">
			<dock index="0" id="80" port="w" />
			<dock index="1" id="82" port="e" />
		</line>
		<room id="84" name="Research Camp (west of the stone)" x="1344" y="128" w="96" h="64" description="" />
		<line id="85">
			<dock index="0" id="84" port="se" />
			<dock index="1" id="80" port="nw" />
		</line>
		<room id="86" name="The Strange Rock" x="1504" y="128" w="96" h="64" description="This is it.  This is the center of the research camp, in both location and attention.  A pavillion-style tent is hung above and around the mysterious object; a large, jet-black, jagged and irregularly-shaped boulder with small, white lettering covering every inch of it.  Some discarded papers litter the area around it.  The rest of the research camp can be reached north, south, east, and west.">
			<objects>discarded papers|tent|boulder|(Thammodu)</objects>
		</room>
		<line id="92">
			<dock index="0" id="84" port="e" />
			<dock index="1" id="86" port="w" />
		</line>
		<room id="87" name="Research Camp (north of the stone)" x="1504" y="0" w="96" h="64" description="" />
		<line id="91">
			<dock index="0" id="86" port="n" />
			<dock index="1" id="87" port="s" />
		</line>
		<room id="89" name="Caleah Bivouac" x="1664" y="0" w="96" h="64" description="Stationed here in the northeast part of the research camp is the Caleah military bivouac.  As a martial society, the city-state of Caleah handles everything in a military manner.  There are about six large caravan tents here, positioned around a well-lit pyre.  Near the pyre is a rickety wooden bench; an anomaly among the rigid order of the encampment.  The rest of the research camp can be accessed to the west.">
			<objects>clay statue|Smuto Shadowheart|wooden bench|caravan tents|pyre</objects>
		</room>
		<line id="95">
			<dock index="0" id="87" port="e" />
			<dock index="1" id="89" port="w" />
		</line>
		<line id="64">
			<dock index="0" id="84" port="ne" />
			<dock index="1" id="87" port="sw" />
		</line>
		<room id="90" name="Research Camp (east of the stone)" x="1664" y="128" w="96" h="64" description="" />
		<line id="93">
			<dock index="0" id="87" port="se" />
			<dock index="1" id="90" port="nw" />
		</line>
		<line id="94">
			<dock index="0" id="90" port="w" />
			<dock index="1" id="86" port="e" />
		</line>
		<line id="88">
			<dock index="0" id="86" port="s" />
			<dock index="1" id="80" port="n" />
		</line>
		<line id="96">
			<dock index="0" id="90" port="sw" />
			<dock index="1" id="80" port="ne" />
		</line>
		<room id="97" name="Porthca Encampment" x="1664" y="256" w="96" h="64" description="A few caravan tents surrounding a well-made pyre burning above the ice.  This encampment also features large rugs with colorful and tangled designs.  It gives the whole encampment a warm, homey feel.  The rest of the research camp can be reached to the north.">
			<objects>rugs|Domon|Cassara</objects>
		</room>
		<line id="100">
			<dock index="0" id="90" port="s" />
			<dock index="1" id="97" port="n" />
		</line>
		<room id="98" name="Kirjath-Huzoth Chalet" x="1344" y="0" w="96" h="64" description="This is a tidy and well-kept encampment on the northwest portion of the research camp.  A few caravan tents, fastidiously maintained, surround a large, warm pyre.  The rest of the research camp can be accessed to the south." />
		<line id="103">
			<dock index="0" id="84" port="n" />
			<dock index="1" id="98" port="s" />
		</line>
	</map>
	<settings>
		<colors>
			<canvas>White</canvas>
			<fill>White</fill>
			<border>MidnightBlue</border>
			<line>MidnightBlue</line>
			<selectedLine>SteelBlue</selectedLine>
			<hoverLine>DarkOrange</hoverLine>
			<largeText>MidnightBlue</largeText>
			<smallText>MidnightBlue</smallText>
			<lineText>MidnightBlue</lineText>
			<grid>WhiteSmoke</grid>
		</colors>
		<fonts>
			<room size="12">Arial</room>
			<object size="11">Arial</object>
			<line size="9">Arial</line>
		</fonts>
		<grid>
			<snapTo>yes</snapTo>
			<visible>yes</visible>
			<showOrigin>yes</showOrigin>
			<size>32</size>
		</grid>
		<lines>
			<width>2</width>
			<handDrawn>no</handDrawn>
			<arrowSize>12</arrowSize>
			<textOffset>4</textOffset>
		</lines>
		<rooms>
			<darknessStripeSize>24</darknessStripeSize>
			<objectListOffset>4</objectListOffset>
			<connectionStalkLength>32</connectionStalkLength>
			<preferredDistanceBetweenRooms>64</preferredDistanceBetweenRooms>
		</rooms>
		<ui>
			<handleSize>12</handleSize>
			<snapToElementSize>16</snapToElementSize>
		</ui>
		<keypadNavigation>
			<creationModifier>control</creationModifier>
			<unexploredModifier>alt</unexploredModifier>
		</keypadNavigation>
	</settings>
</trizbort>